export interface MoviedbResponse extends Response {
  results?: Movie[];
}

export interface Genre {
  id: string;
  name: string;
}

export interface Movie {
  id: string;
  vote_count: number;
  video: boolean;
  vote_average: number;
  title: string;
  poster_path: string;
  original_language: string;
  original_title: string;
  backdrop_path: string;
  adult: boolean;
  overview: string;
  release_date: string;
  genre_ids: number[];
  genres?: Genre[];
}

export interface RootState {
  popularMovies: Movie[];
  selectedMovies: Movie[];
  genreFilter?: number[];
  movie: Movie | null;
}
