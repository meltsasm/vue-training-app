import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import store from '@/store';
import Favourite from '@/views/Favourites.vue';
import MovieDetails from '@/views/MovieDetails.vue';
import {Movie} from '@/models/models';

Vue.use(Router);
export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/favourite',
      name: 'favourite',
      component: Favourite,
    },
    {
      path: '/movie/:id',
      name: 'movie',
      component: MovieDetails,
      beforeEnter: async (to: any, from: any, next: (redirect?: string) => void) => {
        const id = to.params.id;
        const popularMovies: Movie[] = store.getters.getPopularMovies;
        const movie = popularMovies.find((m) => `${m.id}` === id);
        if (movie) {
          store.dispatch('setMovie', movie);
        } else {
          const success = await store.dispatch('getMovieDetails', id);
          if (!success) {
            next('/404');
          }
        }
        next();
      },
    },
    {
      path: '*',
      name: 'error',
      component: () => import('./views/Error.vue'),
    },
  ],
});
